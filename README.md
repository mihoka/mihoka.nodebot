# Mihoka-nodebot
To be defined

## TODO-List for mihoka-nodebot
### Command
### CommandAlias
* Make an alias command possible
### RateLimiter
* Implement self-updating status of 'am I a moderator or not'
* Change the system logic - The IRC Client will create the Queue on init, and the Queue will create the RateLimiter. Once it is created, with default values, it will be called on a time basis to poll messages. If any message is of high importance (priority queue), it will be polled first; If no priority message is there, it will just send the next message to send, to the RAW command. To know if an item is queuable or not, it will be called from its original function in IRC Client, and, if he fits the prerequisites to print a message, it will be queued.
### Queue System
* Add a way to handle the message type if the content can be merged with others (command level handling ?);
### Irc
* Integrate RateLimiter and Queue classes inside Irc
* Enhance QueueObject class (inside Queue class) to make it generic, calling the proper Irc method
* Verify that the client is actually connected to a server and channel before polling any message to send
* When a major command is called such as NICK, PASS, and the rate limit is reached, recall same method with a 500ms delay, until successful.
* replace any function sending a this.raw command, instead enqueue them. We need also to handle the "instant action" or high-priority one for JOIN/PART etc. We should also handle the case there is a message for a channel that we might have left already, so filtering queue on call or use object instead of string ?
### Helper
* Create the isModerator command to know if a specified user+tags provides the Moderator status, Broadcaster status, or any higher.
* Create the helper function that allows to know the group power of a user
### Logic
* Think about how to implement modules inside the Pages, maybe a 'module' page that gets settings, fields, field types, etc. from JSON File
### Logging
* create a logging tool for errors, warnings, debugs. (Not messages, they cannot be saved / ToS - EULA related)
### (other)
* Rewrite all tests
* Rework the Queue system to allow cumulable messages (two persons registered to the giveaway, the bot says only one message with both names in it)
* **Continuous addition** Add some functions to objects such as Command.setNoDelay() or Queue.isJoinQueued() to make the constructor less massive
### Localization
* use a channel-method called *_translationFor_* which will pull the message with the key provided, in the channel language
* Create JSON objects for each modules and make it so the translations here are also used on the future website
* Use the :kv: format for variable names
* Create a Localization class who gets keys, and provided an object array, replaces KEYS found by VALUE provided in the object ('hello :user: !' and {user: 'marc'} would give 'Hello marc !')
### Existing badges for messages 
* admin, 
* bits, 
* broadcaster, 
* global_mod, 
* moderator, 
* subscriber, 
* staff, 
* turbo