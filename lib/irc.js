const colors = require('colors');
const Helper = require('./../modules/helper');
const net = require('net');
const global_conf = require('./../config');
const RateLimiter = require('./../classes/irc/ratelimiter');
const Queue = require('./../classes/irc/queue');

// Fill a new parameter 'serverState' who stocks everything about the current channels.
class Irc {
  constructor(channelObject) {
    this.username = null;
    this.password = null;
    this.channel = channelObject || null;
    this.isMod = false;
    this.host = '';

    this.authenticated = false;
    this.connected = false;
    this.reconnect = true;

    this.requiredCapabilities = [
      'twitch.tv/membership',
      'twitch.tv/commands',
      'twitch.tv/tags'
    ];
    this.enqueuedCapabilities = [...this.requiredCapabilities];
    this.capabilities = { successful: [], failed: [] };

    this.bufferedMessage = null;
    this.socket = null;

    this.queue = new Queue(
      new RateLimiter(30, 30, false, false, true, true, true)
    );
    this.activity = {
      last_answer: 0,
      ping: 0,
      ticker: 0,
      count: 0,
    };
  };
  /**
   * Creates the socket for a new connection to be established
   * @returns {void} nothing
   * @version 1.0
   */
  createSocket() {
      this.socket = new net.Socket();
      this.socket.setEncoding('utf8');
      this.socket.setNoDelay();

      this.socket.on('error', (err) => {
        this.onError(err);
      });
      this.socket.on('close', () => {
        this.onClose();
      });
      this.socket.on('timeout', () => {
        this.onTimeout();
      });
      this.socket.on('end', () => {
        this.onEnd();
      })
      this.socket.on('data', (data) => {
        this.onData(data);
      });
      this.socket.on('connect', () => {
        this.onConnect();
      });
    }
    /**
     * Joins a specified channel, either by name (string) or object (Channel class instance)
     * @param {Channel} channel the channel to join
     * @returns {void} nothing
     * @version 1.0
     */
  join(channel) {
    //TODO: RateLimiter & Enqueue
    this.raw('JOIN ' + channel);
    if (this.queue.rateLimiter.join) { this.queue.rateLimiter.logSending(); }
    return true;
  };
  /**
   * leaves a specified channel, either by name (string) or object (Channel class instance)
   * @param {Channel} channel the channel to leave
   * @returns {void} nothing
   * @version 1.0
   */
  part(channel) {
    //TODO: RateLimiter & Enqueue
    if (this.queue.rateLimiter.join && !this.isValid()) {
      this.queue.enqueue('part', [channel]);
    }
    this.raw('PART ' + channel);
  };
  /**
   * sends a message to a specific channel, without verifying its integrity.
   * @param {Channel} channel the channel where the message will be sent
   * @param {String} message the message to send to the channel
   * @returns {void} nothing
   * @version 1.0
   */
  sendMessage(channel, message) {
    this.raw('PRIVMSG ' + channel + " :" + message);
  };
  /**
   * Sends a /me command to the channel, with 'message' as an action.
   * @param {Channel} channel the channel where the action will be sent
   * @param {String} action the action to display 
   * @returns {void} nothing
   * @version 1.0
   */
  action(channel, action) {
    //TODO: RateLimiter & Enqueue
    this.sendMessage(channel, ".me " + action);
  };
  /**
   * Sends a private message to a user specifically, and not a whole channel
   * @param {User} user the user to contact privately
   * @param {String} message the message to send to the user
   * @returns {void} nothing
   * @version 1.0
   */
  whisper(user, message) {
    //TODO: RateLimiter & Enqueue
    this.raw("PRIVMSG #jtv :/w " + user + " " + message);
  };
  /**
   * Sends raw data of your choice to the server
   * @param {String} data the raw data to send, in UTF-8
   * @returns {void} nothing
   * @version 1.0
   */
  raw(data) {
    if (this.socket && !this.socket.destroyed && this.connected);
    this.socket.write(data + '\n', 'utf8', () => {
      console.log(Date.now() + " MHK-IRC: ".yellow, 'SENT -'.black.bgGreen, data);
      this.queue.triggerSending();
    });
  };
  /**
   * handles the raw data received by the socket form the server, to dispatch it to 'on()' methods of the class. 
   * @param {String} raw the raw data received
   * @returns {void} nothing
   * @version 1.0
   */
  handleRawData(raw) {
    this.activity.last_answer = Date.now();
    // Commande PING ou PONG
    if (raw.substr(0, 4) === "PING" || raw.substr(0, 4) === "PONG") {
      this.onPing(raw);
      return;
    }

    var fchar = raw.substr(0, 1);
    switch (fchar) {
      case ":":
        this.handleMessage(raw);
        break;
      case "@":
        this.handleTagMessage(raw);
        break;
      default:
        this.onUnknown(raw);
        break;
    }
  };
  /**
   * handles a message sent from the server to the socket, which has a IRCv3 tag attached to it.
   * @param {String} raw the raw data to handle
   * @returns {void} nothing
   * @version 1.0
   */
  handleTagMessage(raw) {
    // Population des TAGS
    let rawSplit = raw.split(' ');
    let rawTags = rawSplit[0].toString().substring(1).split(';');
    let tagsParsed = {};
    for (let i = 0; i < rawTags.length; i++) {
      let keyValuePair = rawTags[i].split("=");
      if (keyValuePair.length > 1) {
        tagsParsed[keyValuePair[0].toString()] = keyValuePair[1].toString();
      } else {
        tagsParsed[keyValuePair[0].toString()] = '';
      }
    }
    // Population de l'objet message
    let message = {
      tags: tagsParsed,
      sender: rawSplit[1].toString().substring(1),
      type: rawSplit[2].toString(),
      channel: rawSplit[3].toString(),
    };
    switch (message.type) {
      case "PRIVMSG":
        message.message = rawSplit.slice(4).join(' ').toString().substring(1);
        this.onMessage(message.sender.split('!')[0].toString(), message.channel, message.message, message.tags);
        break;
      case "CLEARCHAT":
        message.target = rawSplit.slice(4).join(' ').toString().substring(1);
        this.onClear(message.channel, message.target, message.tags);
        break;
      case "ROOMSTATE":
        this.onRoomState(message.channel, message.tags);
        break;
      case "NOTICE":
        message.message = rawSplit.slice(4).join(' ').toString().substring(1);
        this.onNotice(message.channel, message.message, message.tags);
        break;
      case "USERSTATE":
        this.onUserState(message.channel, message.tags);
        break;
      default:
        this.onUnknown(raw);
        break;
    }
  };
  /**
   * Handles messages sent without any IRCv3 tags attached to it, and dispatch it around
   * @param {String} raw the message sent without IRCv3 tags
   * @returns {void} nothing
   * @version 1.0
   */
  handleMessage(raw) {
    // Division du message par espaces
    var rawSplit = raw.split(' ');
    var message = {
      sender: rawSplit[0].toString().substring(1),
      type: rawSplit[1].toString(),
      target: rawSplit[2].toString(),
    };

    switch (message.type) {
      case "JOIN":
        // sender = justinfan2402852305!justinfan2402852305@justinfan2402852305.tmi.twitch.tv
        this.onJoin(message.sender.split('!')[0].toString(), message.target);
        break;
      case "PART":
        this.onPart(message.sender.split('!')[0].toString(), message.target);
        break;
      case "HOSTTARGET":
        message.hostName = rawSplit[3].toString().substring(1);
        message.viewers = rawSplit[4].toString();
        this.onHostTarget(message.target, message.hostName, message.viewers);
        break;
      case "CAP":
        this.onCapacity(raw);
        break;
      case "MODE":
        message.mode = rawSplit[3].toString();
        message.modeTarget = rawSplit[4].toString();
        this.onMode(message.modeTarget, message.mode, message.target);
        break;
      case "PRIVMSG":
        this.onMessage(message.sender.split('!')[0].toString(), message.target, rawSplit[3].toString().substring(1));
        break;
      default:
        if (!isNaN(message.type)) {
          // numérique
          message.message = rawSplit.slice(3).join(' ').toString().substring(1);
          this.onWelcome(message.type, message.sender, message.message);
        } else {
          // Mais c'est quoi alors?
          this.onUnknown(raw.toString());
        }
        break;
    }
  };
  /**
   * Handles the arrival of a user joining a room the socket is listening to
   * @param {User} user the user which joined a channel
   * @param {Channel} channel the channel the user joined
   * @returns {void} nothing
   * @version 1.0
   */
  onJoin(user, channel) {
    console.log(Date.now() + " MHK-IRC/JOIN".yellow, (user).green, (channel).cyan);
  };
  /**
   * Handles the leaving of a user from a specific channel
   * @param {User} user 
   * @param {Channel} channel 
   * @returns {void} nothing
   * @version 1.0
   */
  onPart(user, channel) {
    console.log(Date.now() + " MHK-IRC/PART".yellow, (user).green, (channel).cyan);
  };
  /**
   * Handles the changes of status from a user inside a channel, such as being granted or removed the Operator right
   * @param {User} user the user who got a status change
   * @param {String} type the type of change applied to the user
   * @param {*} channel the channel where the user had a status change
   */
  onMode(user, type, channel) {
    console.log(Date.now() + " MHK-IRC/MODE".yellow, (user).green, (type).cyan, (channel).green);
  };
  /**
   * Handles a capacity request or acquiring from the server
   * @param {String} raw the raw data received
   * @returns {void} nothing
   * @version 1.0
   */
  onCapacity(raw) {
    console.log(Date.now() + " MHK-IRC/CAP ACK".yellow, raw);
    const reg = /^:tmi.twitch.tv CAP \* (ACK|NAK) :(\S+)/i;
    if (reg.test(raw)) {
      const capObtained = reg.exec(raw);
      if (capObtained === null) {
        console.log('MHK-IRC/onCapacity - UNHANDLED ERROR'.red);
        return;
      }
      this.capabilities[(capObtained[1] === 'ACK' ? 'successful' : 'failed')].push(capObtained[2]);
    } else {
      console.error('MHK-IRC/onCapacity - BAD REQUEST - PLEASE CHANGE REGEX FORMAT: '.red, raw)
    }

    if (this.requiredCapabilities.length ===
      (this.capabilities.successful.length + this.capabilities.failed.length) &&
      this.enqueuedCapabilities.length === 0) {
      // The capabilities have all been claimed (or attempted to be) we can now join the channel
      if (this.channel && this.channel.name) {
        console.log('MHK-IRC/onCapacity - '.green, 'Attempting to join the channel: '.yellow, (this.channel.name + '').blue);
        this.join(this.channel.name);
      } else {
        console.log('MHK-IRC/onCapacity - '.green, 'No channel name provided. Aborting connection.'.red);
        this.disconnect();
      }
    }
  };
  /**
   * Handles a ping sent from the server to our IRC socket
   * @param {String} raw the ping request, in a raw format
   * @returns {void} nothing
   * @version 1.0
   */
  onPing(raw) {
      console.log(Date.now() + " MHK-IRC/Ping".yellow, raw);
      this.activity.ping = Date.now();
      this.pong(raw);
    }
    /**
     * Handles a PONG answer from the server to our IRC socket, after we requested a PING 
     * @param {String} raw the raw response from the server
     * @returns {void} nothing
     * @version 1.0
     */
  onPong(raw) {
      console.log(Date.now() + " MHK-IRC/Pong".yellow, raw);
    }
    /**
     * Handles any non-handled line by other filters, as the format is unknown or incorrect.
     * @param {String} raw_line the unknown line that failed to be parsed
     * @returns {void} nothing
     * @version 1.0
     */
  onUnknown(raw_line) {
    console.log(Date.now() + " MHK-IRC/UNKNOWN".red.underline, raw_line)
  };
  /**
   * Handles any single PRIVMSG event from the socket, those being users sending messages to others in a room.
   * @param {User} user the user who sent the message
   * @param {Channel} channel the channel where the message was sent to
   * @param {String} message the message received by the server
   * @param {Object} tags the message tags, who includes the user tags
   */
  onMessage(user, channel, message, tags) {
    // Gestion des commandes, tags, etc.
    console.log(Date.now() + " MHK-IRC/Message".blue, "from " + (user).green, "to " + (channel).green, (message).cyan);
    console.log(Date.now() + " MHK-IRC/Message/Tags".blue, tags);

    //TODO: Analyse entrée pour commandes
    // Command template (!something) has been found
    if (this.channel) {
      this.channel.handleMessage(user, message, tags);
    }
  };
  /**
   * Handles the 'CLEAR' events sent from the irc server, to notice a user or the whole channel got purged or timeout, or even banned
   * @param {User} user the user who was cleared
   * @param {Channel} channel the channel where it happened
   * @param {Object} tags the metadata related to the timeout
   * @returns {void} nothing
   * @version 1.0
   */
  onClear(user, channel, tags) {
    console.log(Date.now() + " MHK-IRC/Clear".yellow, "channel " + (channel).green, "user " + (user).green, "tags ", (tags).green);
  };
  /**
   * Handles the 'ROOMSTATE' events sent from the irc server, to notice channel changes properties (R9KBeta, Slow mode, ...)
   * @param {Channel} channel the channel where it happened
   * @param {Object} tags the metadata related to the user
   * @returns {void} nothing
   * @version 1.0
   */
  onRoomState(channel, tags) {
    console.log(Date.now() + " MHK-IRC/Roomstate".yellow, "channel " + (channel).green, "tags ", (tags).green);
  };
  /**
   * Handles the 'USERSTATE' events sent from the irc server, to notice user changes on a room
   * @param {Channel} channel the channel where it happened
   * @param {Object} tags the metadata related to the user
   * @returns {void} nothing
   * @version 1.0
   */
  onUserState(channel, tags) {
    console.log(Date.now() + " MHK-IRC/Userstate".yellow, "channel " + (channel).green, "tags ", (tags).green);
    // keeping double equals instead of triple equals for string transtyping compare
    this.isMod = tags.mod == 1;
    this.queue.rateLimiter.rate = this.isMod ? 100 : 30;
  };
  /**
   * Handles the MOTD messages from a server, on connection of the socket and successful authentication.
   * @param {Number} code the MOTD code
   * @param {String} server the server name
   * @param {String} message the message sent from the server as a MOTD row
   * @returns {void} nothing
   * @version 1.0
   */
  onWelcome(code, server, message) {
    //console.log(Date.now() + " MHK-IRC/MOTD".yellow, (code).green, (server).yellow, (message).green);
    // TODO: handle various cases of ID's
    // https://www.alien.net.au/irc/irc2numerics.html
    // 001 Welcome 002 server name 003 server uptime 004 self (empty)
    // 353 Reply to NAMES (See RFC) (line of members - 356 end of user in channel
    switch (code) {
      case 1:
      case '001':
        // Welcome, GLHF
        break;
      case 2:
      case '002':
        // Server name
        const nameRegex = /^Your host is (\S+)/i;
        const res = nameRegex.exec(message);
        if (res !== null) {
          this.host = res[1];
        }
        console.log('MHK-IRC/Hostname'.blue, (this.host + '').green);
        break;
      case 3:
      case '003':
        // This server is rather new (uptime)
        break;
      case 353:
      case '353':
        // NAMES list
        break;
      case 366:
      case '366':
        // End of NAMES list
        break;
      case '421':
      case 421:
        // Unknwown command
        this.onUnknown(message);
        break;
      case '375':
      case 375:
        // MOTD Start
        console.log('MHK-IRC/Motd'.yellow, '- starting'.green);
        break;
      case '372':
      case 372:
        // MOTD line
        console.log('MHK-IRC/Motd'.yellow, '- line: '.green, message);
        break;
      case '376':
      case 376:
        // MOTD End
        console.log('MHK-IRC/Motd'.yellow, '- ended'.green);
        this.authenticated = true;
        this.requestCapacities();
        break;
      default:
        // Not handled / To add
    }
  };
  /**
   * Handles the NOTICE messages from the IRC server to which we are connected via the socket
   * @param {Channel} channel the channel where the NOTICE was sent
   * @param {String} message the message sent with the Notice
   * @param {Object} tags the metadata the server sent us with the message
   * @returns {void} nothing
   * @version 1.0
   */
  onNotice(channel, message, tags) {
    console.log(Date.now() + " MHK-IRC/Notice".yellow, "channel " + (channel).green, "||| " + (message).green, " ||| tags ", (tags).green);
    /*
        msg-id
            already_banned	        <user> is already banned in this room.
            already_emote_only_off	This room is not in emote-only mode.
            already_emote_only_on	This room is already in emote-only mode.
            already_r9k_off	        This room is not in r9k mode.
            already_r9k_on	        This room is already in r9k mode.
            already_subs_off	    This room is not in subscribers-only mode.
            already_subs_on	        This room is already in subscribers-only mode.
            bad_host_hosting	    This channel is hosting <channel>.
            bad_unban_no_ban	    <user> is not banned from this room.
            ban_success	            <user> is banned from this room.
            emote_only_off	        This room is no longer in emote-only mode.
            emote_only_on	        This room is now in emote-only mode.
            host_off	            Exited host mode.
            host_on	                Now hosting <channel>.
            hosts_remaining	        There are <number> host commands remaining this half hour.
            msg_channel_suspended	This channel is suspended.
            r9k_off                 This room is no longer in r9k mode.
            r9k_on	                This room is now in r9k mode.
            slow_off	            This room is no longer in slow mode.
            slow_on	                This room is now in slow mode. You may send messages every <slow seconds> seconds.
            subs_off	            This room is no longer in subscribers-only mode.
            subs_on	                This room is now in subscribers-only mode.
            timeout_success	        <user> has been timed out for <duration> seconds.
            unban_success	        <user> is no longer banned from this chat room.
            unrecognized_cmd	    Unrecognized command: <command>
    */
    const reg_already_banned = /^(\S+) is already banned in this room./i;
    const reg_bad_host_hosting = /^This channel is hosting (\S+)./i;
    const reg_bad_unban_no_ban = /^(\S+) is not banned from this room./i;
    const reg_ban_success = /^(\S+) is banned from this room./i;
    const reg_host_on = /^Now hosting (\S+)./i;
    const reg_hosts_remaining = /^There are (\S+) host commands remaining this half hour./i;
    const reg_slow_on = /^This room is now in slow mode. You may send messages every (\d) seconds./i;
    const reg_timeout_success = /^(\S+) has been timed out for (\d) seconds./i;
    const reg_unban_success = /^(\S+) is no longer banned from this chat room./i;
    const reg_unrecognized_cmd = /^Unrecognized command: (.*)/i;
  };
  /**
   * Handles the notice from a remote user that hosted the current channel for a specific amount of spectators
   * @param {User} sender the user who sent the data
   * @param {Channel} target the channel who received the host
   * @param {Number} viewers the amount of viewers for which the stream has been hosted
   * @returns {void} nothing
   * @version 1.0
   */
  onHostTarget(sender, target, viewers) {
    console.log(Date.now() + " MHK-IRC/HOSTTARGET".yellow, (sender).green, "hosted", (target).cyan, "for " + (viewers).green + " viewers.");
  };
  /**
   * Handles the 'timeout' event (when the socket does not receive anything since a specific delay) of the IRC Socket. Allows to do hooks once the event is triggered
   * @returns {void} nothing
   * @version 1.0
   */
  onTimeout() {
    this.connected = false;
    console.error('timeout of connection.');
  };
  /**
   * Handles the 'end' event (when the socket stops listening to any new data from the server) of the IRC Socket. Allows to do hooks once the event is triggered
   * @returns {void} nothing
   * @version 1.0
   */
  onEnd() {
    this.connected = false;
    console.error('The server closed the connection.');
  };
  /**
   * Handles the 'connect' event (when the socket successfully connects to the remote) of the IRC Socket. Allows to do hooks once the event is triggered
   * @returns {void} nothing
   * @version 1.0
   */
  onConnect() {
    console.log(Date.now() + ' Established connection to IRC server');
    this.connected = true;

    this.activity.ping = Date.now();
    this.verifyPingability();

    setTimeout(() => {
      this.authenticate();
    }, 1000);
  };
  /**
   * Handles the 'data' event (when the socket receives input from the server) of the IRC Socket. Allows to do hooks once the event is triggered
   * @param {String} data the data sent from the server to us
   * @returns {void} nothing
   * @version 1.0
   */
  onData(data) {
    if (data.slice(-1) !== '\n') {
      console.log('!!! Message is not terminated, buffering !!!'.black.bgRed);
      if (this.bufferedMessage === null) {
        this.bufferedMessage = '';
      }
      this.bufferedMessage += data;
      return;
    } else {
      if (this.bufferedMessage) {
        data = this.bufferedMessage + data;
        this.bufferedMessage = null;
      }
      data = data.split('\n');
    }
    for (var i = 0; i < data.length; i++) {
      if (data[i] !== '') {
        console.log('RECV -'.bold.black.bgWhite, data[i].red.bgWhite);
        this.handleRawData(data[i].slice(0, -1));
      }
    }
  };
  /**
   * Handles the 'error' event (when the socket cannot handle something and have to stop running) of the IRC Socket. Allows to do hooks once the event is triggered
   * @param {Object} err the error we received
   * @returns {void} nothing
   * @version 1.0
   */
  onError(err) {
    console.error(Date.now() + (" MHK-IRC/Error: ".red) + "Socket error #" + err.code, err.toString());
    this.connected = false;
  };
  /**
   * Handles the 'close' event (when the socket closes itself, by user demand or crash/timeout) of the IRC Socket. Allows to do hooks once the event is triggered
   * @returns {void} nothing
   * @version 1.0
   */
  onClose() {
    console.error(Date.now() + (" MHK-IRC/Error: ".red) + "Disconnected from socket. Attempting to reconnect in 5 seconds.");
    this.connected = false;

    this.socket.destroy();
    this.queue.rateLimiter.reset();
    this.enqueuedCapabilities = [...this.requiredCapabilities];
    this.createSocket();

    if (this.reconnect) {
      setTimeout(() => { this.connect(global_conf.irc.twitch.server.port, global_conf.irc.twitch.server.host); }, 5000);
    } else {
      //
    }
  };
  /**
   * Sends a PONG message to the server 
   * @returns {void} nothing
   * @version 1.0
   */
  pong(ping_req) {
    const regex = /^PING :(\S+)/i;
    if (regex.test(ping_req)) {
      const serverName = regex.exec(ping_req)[1];
      console.error(Date.now() + (" MHK-IRC/Pong: ".green) + serverName);
      this.raw('PONG :' + serverName);
    }
  };
  /**
   * Enables a continuous verification, every minute, of the good health of the socket.
   * Allows to handle events such as a disconnection (which stop the checks), or if the server havent asked any PING in a while.
   * @returns {void} nothing
   * @version 1.0
   */
  verifyPingability() {
      setTimeout(() => {
        if (!this.connected) { return false; }
        const now = Date.now();
        // Checking PINGS
        if (this.socket != null && now > (this.activity.ping + (5.5 * 60 * 1000))) {
          this.raw('PING');
        }
        // Checking NO answer
        if (this.socket != null && now > this.activity.last_answer + 15 * 60 * 1000) {
          this.disconnect();
          this.reconnect = true;
          this.connect();
        }

        if (this.connected) {
          // Relaunch a check if we are connected still
          this.verifyPingability();
        }
      }, 1 * 60 * 1000);
    }
    /**
     * Enqueues a message to be sent later by the IRC User, allowing it to not be shadowbanned.
     * The function name provided allows to do treatments beforehand. 
     * This could be changed to auto-enqueue from each objects if the object is valid ?
     * @param {String} funcName the name of the function to call
     * @param {Array} args the list of arguments to give to the array
     * @returns {void} nothing
     * @version 1.0
     */
  enqueue(funcName, args) {
      this.queue.enqueue(funcName, args);
    }
    /**
     * Enqueues a message to be sent later by the IRC User, allowing it to not be shadowbanned.
     * The actions provided in this function are of high priority and will be handled first.
     * The function name provided allows to do treatments beforehand. 
     * This could be changed to auto-enqueue from each objects if the object is valid ?
     * @param {String} funcName the name of the function to call
     * @param {Array} args the list of arguments to give to the array
     * @returns {void} nothing
     * @version 1.0
     */
  enqueueHighPriority(funcName, args) {
      this.queue.enqueueHighPriority(funcName, args);
    }
    /**
     * defines the Channel to be used while the IRC connection is active
     * @param {Channel} channel the Channel instance to link to the IRC client
     * @returns {void} nothing
     * @version 1.0
     */
  setChannel(channel) {
    // Vérifier si on est connecté, si oui se déconnecter, etc.
    this.channel = channel;
  };
  /**
   * provides the channel used by the IRC connection.
   * @returns {Channel} the related channel
   * @version 1.0
   */
  getChannel() {
    return this.channel;
  };
  /**
   * defines the username to use when connecting to the IRC server
   * @param {String} username the username to use
   * @returns {void} nothing
   * @version 1.0
   */
  setNick(username) {
    // vérifier connexion, bla bla bla
    this.username = username;
  };
  /**
   * provide the NICK used to connect to the IRC server 
   * @returns {String} the username for the server
   * @version 1.0
   */
  getNick() {
    return this.username || global_conf.irc.twitch.user.nick;
  };
  /**
   * defines the password to use when connecting to the IRC Server, in combo with NICK.
   * @param {String} password the password used for connection
   * @returns {void} nothing
   * @version 1.0
   */
  setPass(password) {
    // vérifier connexion, bla bla bla
    this.password = password;
  };
  /**
   * provides the IRC client password. Usually for Twitch, it is an oauth token.
   * @returns {String} the password used to connect to the IRC server
   */
  getPass() {
    return this.password || global_conf.irc.twitch.user.pass;
  };
  /**
   * Sets the configured attribute of the Irc object to val. Not used anymore
   * @param {Boolean} val the value or expression test to check if it is configured
   * @returns {void} nothing
   * @deprecated since version 0.3, it is no longer used
   * @version 1.0
   */
  setConfigured(val) {
    this.config.health.configured = val;
  };
  /**
   * Disconnects the socket from the IRC server gracefully, disabling the reconnect setting
   * @returns {void} nothing
   * @version 1.0
   */
  disconnect() {
    if (this.socket != null) {
      this.reconnect = false;
      this.socket.destroy();
    }
  };
  /**
   * Connects the Irc object to the server. If no username, password or host/port is defined, it will fail.
   * @returns {void} nothing
   * @version 1.0
   */
  connect() {
    if (this.channel === undefined || this.channel.name === undefined) {
      console.error(Date.now(), "Unable to connect if no channel is defined.".red);
      console.error(Date.now(), "cancelling connection attempt...".red);
    } else {
      if (this.socket == null) {
        //TODO: RateLimiter & Enqueue
        this.createSocket();
        this.socket.connect(global_conf.irc.twitch.server.port, global_conf.irc.twitch.server.host);
      }
    }
  };
  /**
   * Checks if the given object is a correct member of the Irc class or not
   * @param {Object} object the object to compare to 
   * @returns {Boolean} true if it is an IRC server, false otherwise
   * @deprecated not useful anymore as there is the instanceof keyword
   * @version 1.0
   */
  isValidClass(object) {
      if (typeof object !== 'function' || Array.isArray(object)) {
        return false;
      }
      const currentKeys = Object.keys(this);
      const objectKeys = Object.keys(object);
      return currentKeys.toString() === objectKeys.toString();
    }
    /**
     * Polls from the queue the next message, and calls a timeout once done
     * @returns {void} nothing
     * @version 1.0
     */
  pollFromQueue() {
      if (this.queue.rateLimiter && this.queue.rateLimiter.per > 0 && this.queue.rateLimiter.isValid() > 0) {
        const rate = this.queue.rateLimiter.rate / this.queue.rateLimiter.per;

        const polled = this.queue.poll();
        if (polled) {
          if (typeof this[polled.func] === 'function') {
            this[polled.func](...(polled.data));
          }
        }
        setTimeout(this.pollFromQueue, rate * 1000);
      } else {
        setTimeout(this.pollFromQueue, 1000);
      }
    }
    /**
     * Authenticates with the IRC Server, taking in charge the quota limit.
     * @returns {void} nothing
     * @version 1.0
     */
  authenticate() {
      if (this.connected && !this.authenticated) {
        if (this.getPass() !== '' && this.queue.getAvailableQuota() >= 2) {
          // User has a password
          console.log('MHK-IRC/Authenticate'.green, 'Authenticating with password...'.yellow);
          this.raw('PASS ' + this.getPass());
          this.raw('NICK ' + this.getNick());
          // TODO: Handle 'MOTD' messages to know if we succeeded to connect. If so, call this.requestCapacities();
        } else if (this.getPass() === '' && this.queue.getAvailableQuota() >= 1) {
          // User has no password
          console.log('MHK-IRC/Authenticate'.green, 'Authenticating with no password...'.yellow);
          this.raw('NICK ' + getNick());
          // TODO: Handle 'MOTD' messages to know if we succeeded to connect. If so, call this.requestCapacities();
        } else {
          // Repeat same method until authenticated, every second
          console.log('MHK-IRC/Authenticate'.yellow, 'Quota is reached, attempting again in 1 second.'.blue);
          setTimeout(() => { this.authenticate(); }, 1000 * 1);
        }
      } else if (!this.connected) {
        console.log('MHK-IRC/Authenticate'.red, 'Unable to authenticate if the client is not connected. Aborting.'.yellow);
      }
    }
    /**
     * Requests to the IRC Server the capacities to see joins and parts, the commands typed and the user tags for management.
     * @returns {void} nothing
     * @version 1.0
     */
  requestCapacities() {
    if (this.connected && this.authenticated) {
      if (this.queue.getAvailableQuota() >= 1 && this.enqueuedCapabilities.length > 0) {
        const capName = this.enqueuedCapabilities.shift();
        console.log('MHK-IRC/requestCapabilities'.yellow, 'Requesting capabilitie: '.green, (capName + '').blue);
        this.raw("CAP REQ :" + capName);
        setTimeout(() => { this.requestCapacities(); }, 100 * 1);
      } else if (this.enqueuedCapabilities.length == 0) {
        console.log('MHK-IRC/requestCapabilities'.yellow, 'There is no capability left to claim. Aborting.'.red);
      } else {
        // Retrying in one second
        console.log('MHK-IRC/requestCapabilities'.red, 'The queue has no availability to do the calls, trying again in one second'.yellow);
        setTimeout(() => { this.requestCapacities(); }, 1000 * 1);
      }
    } else {
      // Authentication wasn't successful, aborting
    }
  }
}

module.exports = Irc;