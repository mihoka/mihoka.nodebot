//const Helper = require('./helper');
const Command = require('./../classes/commands/command');
const Helper = require('./../modules/helper');

class ServiceModule {
  constructor(name, onInit, onStop) {
    this._commands = [];
    this._channel = null;
    this._name = name;
    this._moduleId = null;
    this._enabled = true;
    this.onInit(onInit) || (this._onInit = null);
    this.onStop(onStop) || (this._onStop = null);
    this._before = null;
    this._after = null;
  };

  bindChannel(chan) {
    if (Helper.getType(chan) !== 'Channel') {
      console.error('the binded parameter is not a channel.');
      return false;
    }
    this._channel = chan;
    console.log('channel is now bound to the module.');
    return true;
  }
  getDataKey(key) {
    if (!this._channel) { return null; }
    return this._channel.getModuleData(this._name)[key];
  };
  setDataKey(key, val) {
    if (!this._channel) { return null; }
    if (!key) {
      return false;
    }
    this._channel.getModuleData(this._name)[key] = val;
    return true;
  };
  removeDataKey(key) {
    if (!this._channel) { return null; }
    const pre = Object.keys(this._channel.getModuleData(this._name)).length;
    delete this._channel.getModuleData(this._name)[key];
    const post = Object.keys(this._channel.getModuleData(this._name)).length;
    return pre === post + 1;
  };
  addCommand(command) {
    if (!(Helper.getType(command) === 'Command')) { return false; }
    if (this.getCommandByName(command.name)) { return false; }
    this._commands.push(command);
  };
  removeCommand(command)  {
    let found = -1;
    this._commands.some((item, index) => {
      if (item.isMatching(messageString))  {
        found = index;
        return true;
      }
    });
    if (found < 0) {
      return false;
    }
    return this._commands.splice(found, 1).length > 0;
  };
  sendMessage(message) {
    if (!this._channel)  { return false; }
    if (!this.isEnabled())  { return false; }
    this._channel.sendMessage(message);
  };
  onBefore(func) {
    if (func instanceof Function) {
      this._before = func;
    }
  };
  onAfter(func) {
    if (func instanceof Function) {
      this._after = func;
    }
  };
  before(channel, user, message, tags) {
    if (typeof this._before === 'function') {
      return this._before(channel, this, user, message, tags) || false;
    }
  };
  after(channel, user, message, tags) {
    if (typeof this._after === 'function') {
      return this._after(channel, this, user, message, tags) || false;
    }
  }
  getCommandByRegex(messageString) {
    if (!this.isEnabled())  { console.log('module is disabled.'); return null; }
    let found = null;
    this._commands.some(item => {
      console.log('looping into commands, current is ', item);
      if (item.isMatching(messageString))  {
        console.log('it is matching the regex');
        found = item;
        return true;
      }
    });
    console.log('we returned the found command, ', found);
    return found;
  };
  getCommandByName(unique_name) {
    if (!this.isEnabled())  { return null; }
    const cmd = new Command(unique_name, Command.CMD_TYPE_COMMAND, /^$/i, () => {});
    let found = null;
    this._commands.some(item => {
      if (item.equals(cmd))  {
        found = item;
        return true;
      }
    });
    return found;
  };
  getFQDN() {
    return `MHKNBT_INET64SOCK_::${this._name.toUpperCase()}`;
  };
  getUniqueName() {
    return this._name;
  }
  handleMessage(channel, user, message, tags) {
    if (!this.isEnabled())  { return false; }
    if (!this._channel)  { return false; }
    const command = this.getCommandByRegex(message);
    if (command) {
      console.log('found a command matching: ', command);
      const context = { channel, module: this, tags }
      command.run(context, user, message);
      return true;
    }
    console.log('found no command matching: ', message);
    return false;
  };
  disable() {
    this._enabled = false;
  };
  enable() {
    this._enabled = true;
  };
  isEnabled() {
    return this._enabled;
  }
  onInit(func) {
    if (typeof func !== 'function') {
      return false;
    }
    this._onInit = func;
    return true;
  }
  onStop(func) {
    if (typeof func !== 'function') {
      return false;
    }
    this._onStop = func;
    return true;
  }
  stop() {
    if (!this._onStop) {
      return false;
    }
    this._onStop();
  }
  init() {
    if (!this._onInit) {
      return false;
    }
    this._onInit();
  }
};

module.exports = ServiceModule;