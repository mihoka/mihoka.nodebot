const ServiceModule = require('../serviceModule');
const Command = require('../../classes/commands/command');

// Module methods handling
const onInit = () => {};
const onStop = () => {};
// Module creation
const mdl = new ServiceModule('Mihoka', onInit, onStop);

const mhk_cmd_mihoka = new Command('!mihoka', 'command', /^!mihoka/i, (ctx, user, message) =>  {
    ctx.channel.sendMessage(`Je suis Mihoka, une entité visant à aider les streameurs en fournissant des outils et statistiques pour améliorer leur audience. Je suis actuellement en version 3.0-indev et privée jusqu'à mon arrivée en Alpha, prévue fin Août. Plus d'informations sur https://mihoka.tv/`);
});
// Commands linking to module
mdl.addCommand(mhk_cmd_mihoka);
// Export of the module for use
module.exports = mdl;