const Helper = require('./helper');
const global_config = require('./../config');
const ApiRequest = require('./../classes/api/request');
const Irc = require('./../lib/irc');
require('colors');

class Channel {
  constructor(uname) {
    // channel name, with leading #
    this.name = uname || '';
    // IRC Client module to communicate
    this.irc = null;
    // User custom commands
    this.customs = [];
    // Channel database unique ID for API calls
    this.id = 0;
    // MihokaTV Channel ID
    this.id = null;
    // Channel settings as a key-value pair. Name standardized to uppercase
    this.settings = {};
    // serviceModules list for the channel
    this.modules = [];
    // Custom data for modules, on channel level for other modules to use the data too
    this.modulesData = {};
  }
  set name(username) {
    if (username) {
      this._name = username.length > 0 && username.substring(0, 1) === '#' ? username : '#' + username;
    } else {
      this._name = '';
    }
  };
  get modules() {
    return this._modules;
  };
  set modules(val) {
    if (!Array.isArray(val)) {
      return false;
    }

    let validArray = true;
    val.some(itm => {
      if (Helper.getType(itm) !== 'ServiceModule') {
        validArray = false;
        return true;
      }
    });

    if (validArray) {
      this._modules = [...val];
    }
    return validArray;
  };
  get name() {
    return this._name;
  };
  // --- Activity
  addSetting(rule, value) {
    if (typeof rule !== 'string' ||
      (typeof value !== 'string' &&
        typeof value !== 'boolean')) {
      return false;
    }
    this.settings[rule.toString().toUpperCase()] = value;
    return true;
  };
  removeSetting(rule) {
    if (typeof this.settings !== 'object' || Array.isArray(this.settings)) {
      this.settings = {};
      return false;
    }
    if (!rule || typeof rule !== 'string' || rule.length == 0) {
      return false;
    }
    if (!this.settings[rule.toString().toUpperCase()]) {
      return false;
    }
    delete this.settings[rule.toString().toUpperCase()];
    return true;
  };
  getSetting(rule) {
    if (typeof this.settings !== 'object' || Array.isArray(this.settings)) {
      this.settings = {};
      return false;
    }
    return this.settings[rule.toString().toUpperCase()] || null;
  };
  /**
   * define the Irc client to use with channel
   * @param {Irc} _irc the IRC client object to link
   */
  setIrc(_irc) {
    if (!_irc || !(Helper.getType(_irc) === 'Irc')) {
      return false;
    }
    if (this.irc === null) {
      this.irc = _irc;
      return true;
    }
    return false;
  };
  setupIrc() {
    if (this.irc !== null) {
      if (this.getSetting('IRC_NICK') && this.getSetting('IRC_PASS')) {
        this.irc.setNick(this.getSetting('IRC_NICK'));
        this.irc.setPass(this.getSetting('IRC_PASS'));
      } else {
        this.irc.setNick(global_config.irc.twitch.user.nick);
        this.irc.setPass(global_config.irc.twitch.user.pass);
      }
      return true;
    }
    return false;
  };
  disconnectIrc() {
    this.irc.disconnect();
  }
  connectIrc() {
    this.irc.connect();
  }
  isIrcConnected() {
    return this.irc && this.irc.connected;
  }
  connect() {
    // todo: add fetch data from API
    if (!this.irc) {
      let ircSocket = new Irc(this);
      this.setIrc(ircSocket);
      this.setupIrc();
      this.connectIrc();
    }
  };
  sendMessage(message) {
    if (this.isIrcConnected() && !this.getSetting('MHK_MODE_SILENT')) {
      this.irc.sendMessage(this.name, message);
    }
  };
  // --- commands
  addCommand(command) {
    if (!command.name &&
      (command.type === 'command' || command.type == 'trigger') &&
      typeof command.run === 'function' &&
      !command.call && !command.delayed && !command.regex) {
      if (!this.findCommandByName(command.name)) {
        this.commands.push(command);
        return true;
      }
    }
    return false;
  };
  removeCommand(commandName) {
    __index = this.findCommandIndex(commandName);
    if (__index >= 0) {
      this.commands.splice(__index, 1);
      return true;
    }
    return false;
  };
  findCommandByName(commandName) {
    __index = this.findCommandIndex(commandName);
    return this.commands[__index] || null;
  };
  findCommandIndex(commandName) {
    let indexFound = -1;
    this.commands.some((item, index) => {
      if (item.name === commandName) {
        indexFound = index;
        return true;
      }
    });
    return indexFound;
  };
  findCommandByRegex(message) {
    let __commande = Helper.findCommandByRegex(message, nativeCommands);
    return __commande || Helper.findCommandByRegex(message, this.commands);
  };
  isCommand(message) {
    return !!this.findCommandByRegex(message);
  };
  updateCommand(commandName, changedElements) {
    let index = this.findCommandIndex(commandName);
    if (index !== -1) {
      // TODO: Code that. We get each key of changedElements and inject its value into the object found.
      let command = this.commands[index];
      this.removeCommand(commandName);
      Object.keys(changedElements).forEach((item) => {
        command[item] = changedElements[item];
      });
      this.addCommand(command);
      return true;
    }
    return false;
  }
  clearCommands() {
    this.commands = [];
  };
  getModule(name) {
    // returns the found module depending on the string provided 
    let found = null;
    this.modules.some(itm => {
      if (itm._name === name) {
        found = itm;
        return true;
      }
    });
    console.log('getmodule returned ', found);
    return found;
  }
  addModule(srvmodule) {
    if (Helper.getType(srvmodule) === 'ServiceModule') {
      srvmodule.bindChannel(this);
      console.log('initializing module: '.yellow, (srvmodule._name).cyan);
      srvmodule.init();
      console.log('before: ', this.modules.length, this.modules);
      this.modules.push(srvmodule);
      console.log('after: ', this.modules.length, this.modules);
      console.log((srvmodule._name).cyan, 'initialized.'.yellow);
    }
  }
  handleMessage(user, message, tags) {
    // Calling the Before() of each modules
    this.modules.forEach(itm => {
      if (itm.isEnabled()) {
        console.log(itm._name, 'before');
        itm.before(this, user, message, tags);
        console.log(itm._name, 'end of before');
      }
    });
    // Seeking a command
    this.modules.some(itm => {
      if (itm.isEnabled() && itm.handleMessage(this, user, message, tags)) {
        console.log(itm._name, 'handled message to true');
        return true;
      }
    });
    // Processing the after() of each modules
    this.modules.forEach(itm => {
      if (itm.isEnabled()) {
        console.log(itm._name, 'after');
        itm.after(this, user, message, tags);
        console.log(itm._name, 'end of after');
      }
    });
  }
  getModuleData(moduleName) {
    const mdl = this.getModule(moduleName);
    if (mdl) {
      const moduleData = this.modulesData[mdl.getUniqueName()];
      return moduleData;
      // TODO: Check databinding when using this method
    }
    return null;
  }
  setModuleData(moduleName, moduleData) {
    const c_module = this.getModule(moduleName);
    if (c_module) {
      console.log('setmoduledata', this.modulesData[c_module.getUniqueName()]);
      this.modulesData[c_module.getUniqueName()] = moduleData;
      console.log('setmoduledata', this.modulesData[c_module.getUniqueName()]);
      return true;
    }
    console.log('not found the module...');
    return false;
  }
  update() {
    // sends current data to API Server for update
    API.Mihoka.updateChannel(this).then(answer => {
      //
    }).catch(err => {
      //
    });
  }
};

module.exports = Channel;