const Raffle = require('./raffle');
const ServiceModule = require('./../serviceModule');
const Command = require('./../../classes/commands/command');

// Module methods handling
const onInit = () => {};
const onStop = () => {};
// Module creation
const mdl = new ServiceModule('Raffle', onInit, onStop);

const cmd_raffle = new Command('!raffle', 'command', /^!raffle/i, (ctx, user, message) =>  {
  let current = ctx.module.getDataKey('current');
  if (current && current.isActive) {
    // Raffle is currently active (accepting registrations)
    const isRegistered = current.register(user);
    if (isRegistered) {
      ctx.module.sendMessage(user.display + ', vous êtes enregistré pour le Raffle!');
    }
  }
});

const cmd_raffle_draw = new Command('!raffle draw', 'command', /^raffle draw/i, (ctx, user, message) => {
  let current = ctx.module.getDataKey('current');
  if (current) {
    const result = current.draw();
    if (result) {
      let outcome = 'Le gagnant du Raffle est ' + result.display;
      if (ctx.module.getDataKey('checkFollowOnDraw')) {
        const isFollowing = API.Twitch.isFollowing(user.id, ctx.channel.id);
        if (isFollowing) {
          outcome += ', et suit la chaîne.';
        } else {
          outcome += ', mais ne suit pas la chaîne.';
        }
      }
      ctx.channel.update();
      ctx.module.sendMessage(outcome);
    }
  }
});

const cmd_raffle_cancelregistration = new Command('!noraffle', 'command', /^!noraffle/i, (ctx, user, message) => {
  let current = ctx.module.getDataKey('current');
  if (current && current.isActive) {
    const isRemoved = current.unregister(user);
    if (isRemoved) {
      ctx.module.sendMessage(user.display + ', vous êtes désormais désinscrit du Raffle.');
    }
  }
});

const cmd_raffle_register = new Command('!raffle start', 'command', /^!raffle start/i, (ctx, user, message) => {
  let current = ctx.module.getDataKey('current');
  if (!current) {
    // there is no current raffle
    ctx.module.setDataKey('current', new Raffle());
  } else {
    // There is a current raffle, checking the previous raffle list
    let previousRaffles = ctx.module.getDataKey('previous');
    if (!previousRaffles) {
      ctx.module.setDataKey('previous', [current]);
    }
  }
  ctx.module.sendMessage('Le Raffle est lancé! pour participer, il vous suffit de taper !raffle');
});

// Commands linking to module
cmd_raffle.addSubCommand(cmd_raffle_draw);
cmd_raffle.addSubCommand(cmd_raffle_register);
cmd_raffle.addSubCommand(cmd_raffle_cancelregistration);
mdl.addCommand(cmd_raffle);
// Export of the module for use
module.exports = mdl;