const Helper = require('./../helper');
class Raffle {
  constructor() {
    this.startDate = Date.now();
    this.members = [];
    this.isActive = true;
    this.winners = [];
    this.endDate = null;
    this.id = null;
  };
  register(user) {
    const foundIndex = Helper.arrayObjectIndex(user, this.members, 'id');
    if (this.isActive && !foundIndex === -1) {
      this.members.push(user);
      return true;
    }
    return false;
  }
  unregister(user) {
    const foundIndex = Helper.arrayObjectIndex(user, this.members, 'id');
    if (this.isActive && foundIndex !== -1) {
      this.members.splice(foundIndex);
      return true;
    }
    return false;
  }
  draw() {
    if (this.members.length > 0) {
      const maxDraw = this.members.length - 1;
      const generatedNumber = Helper.randomize(0, maxDraw);
      const winner = this.members.splice(generatedNumber, 1);
      this.winners.push(winner);
      this.isActive = false;
      if (!this.endDate) {
        this.endDate = Date.now();
      }
      return winner;
    }
    return null;
  }
}

module.exports = Raffle;