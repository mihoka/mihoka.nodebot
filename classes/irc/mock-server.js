const global_settings = require('./../../config');
const Socket = require('net').Socket;
class MockIrcServer {
  constructor() {
    this.sock = new Socket();
    this.sock.setNoDelay();
    this.sock.setEncoding('utf8');
  }
}

module.exports = MockIrcServer;