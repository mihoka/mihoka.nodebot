/**
 * RateLimiter
 * this class allows the IRC client to queue himself on sending messages if the service is about to be 
 * out of sending rate
 */
module.exports = class RateLimiter {
  /**
   * Creates a rate limiter that provides informations to IRC Clients to know if they are reaching the limit or not
   * @param {Number} rate the number of messages to send at maximum, on a 'per' seconds period
   * @param {Number} per time, in second, in which we are limited to send 'rate' messages
   * @param {Boolean} countJoinPart if the JOIN and PART commands are part of the rate limit
   * @param {Boolean} countConnect if the connection orders (NICK/PASS) are part of the rate limit
   * @param {Boolean} countMessage if the messages are part of the rate limit
   * @param {Boolean} countAction  if the /action /me commands have to be part of the rate limit
   * @param {Boolean} countSrvCommands if the commands sent to server have to be part of the rate limit
   */
  constructor(rate, per, countJoinPart, countConnect, countMessage, countAction, countSrvCommands) {
    this.rate = rate;
    this.per  = per;
    this.join = countJoinPart;
    this.conn = countConnect;
    this.mess = countMessage;
    this.act  = countAction;
    this.srv  = countSrvCommands;
    this.messages = [];
  }

  /**
   * gives the number of messages allowed to be sent in a specific duration
   * @returns {Number} the number of messages allowed on the defined time window
   */
  get rate() {
    if(!this._rate) {
      this._rate = 30;
    }
    return this._rate;
  }
  /**
   * sets the value of the rate
   * @param {Number} val the number of messages allowed in a specific duration delay
   * @returns {Number} the value given and set, as a Integer, or 0 if the number is not valid.
   */
  set rate(val) {
    if(!this._rate) {
      this._rate = 30;
    }

    if(typeof val !== 'number') {
      return 0;
    }
    const value = Number.parseInt(val);
    if(value <= 0) {
      return 0;
    }
    this._rate = value;
    return value;
  }
  /**
   * provides the duration window for the messages to be sent
   * @returns {Number} the time in which the 'rate' messages have to be sent, in seconds
   */
  get per() {
    if(!this._per) {
      this._per = 30;
    }
    return this._per;
  }
  /**
   * sets the number of seconds in which the 'rate' messages can be sent
   * @param {Number} val the number of seconds for the time window
   * @returns {Number} the value provided as a Integer if it is valid, 0 otherwise
   */
  set per(val) {
    if(!this._per) {
      this._per = 30;
    }
    if(typeof val !== 'number') {
      return 0;
    }
    const value = Number.parseInt(val);
    if(value <= 0) {
      return 0;
    }
    this._per = value;
    return value;
  }
  /**
   * whether the JOIN and PART requests are counting towards the rate limitation
   * @returns {Boolean} true if JOIN and PART counts, false otherwise 
   */
  get join() {
    if(this._join === undefined) {
      this._join = true;
    }
    return this._join;
  }
  /**
   * sets the informative attribute specifying whether the JOIN and PART commands are part of the rate limit
   * @param {Boolean} val if the JOIN and PART commands have to be counted or not
   * @returns {Boolean} the new value of the attribute, or null if the value provided is invalid
   */
  set join(val) {
    if(this._join === undefined){
      this._join = true;
    } 
    if(typeof val !== 'boolean') {
      return null;
    }
    this._join = val;
    return val;
  }
  /**
   * whether the connection requests such as NICK and PASS are counting towards the rate limitation
   * @returns {Boolean} true if connection requests counts, false otherwise 
   */
  get conn() {
    if(this._conn === undefined) {
      this._conn = true;
    }
    return this._conn;
  }
  /**
   * sets the informative attribute specifying whether the connection request commands are part of the rate limit
   * @param {Boolean} val if the connection requests commands have to be counted or not
   * @returns {Boolean} the new value of the attribute, or null if the value provided is invalid
   */
  set conn(val) {
    if(this._conn === undefined){
      this._conn = true;
    } 
    if(typeof val !== 'boolean') {
      return null;
    }
    this._conn = val;
    return val;
  }
  /**
   * whether the messages are counting towards the rate limitation
   * @returns {Boolean} true if messages counts, false otherwise 
   */
  get mess() {
    if(this._mess === undefined) {
      this._mess = true;
    }
    return this._mess;
  }
  /**
   * sets the informative attribute specifying whether the messages are part of the rate limit
   * @param {Boolean} val if the messages have to be counted or not
   * @returns {Boolean} the new value of the attribute, or null if the value provided is invalid
   */
  set mess(val) {
    if(this._mess === undefined){
      this._mess = true;
    } 
    if(typeof val !== 'boolean') {
      return null;
    }
    this._mess = val;
    return val;
  }
  /**
   * whether the ACTION commands are counting towards the rate limitation
   * @returns {Boolean} true if ACTION commands counts, false otherwise 
   */
  get act() {
    if(this._act === undefined) {
      this._act = true;
    }
    return this._act;
  }
  /**
   * sets the informative attribute specifying whether the ACTION commands are part of the rate limit
   * @param {Boolean} val if the ACTION commands have to be counted or not
   * @returns {Boolean} the new value of the attribute, or null if the value provided is invalid
   */
  set act(val) {
    if(this._act === undefined){
      this._act = true;
    } 
    if(typeof val !== 'boolean') {
      return null;
    }
    this._act = val;
    return val;
  }
  /**
   * whether the server commands are counting towards the rate limitation
   * @returns {Boolean} true if the server commandscounts, false otherwise 
   */
  get srv() {
    if(this._srv === undefined) {
      this._srv = true;
    }
    return this._srv;
  }
  /**
   * sets the informative attribute specifying whether the server commands are part of the rate limit
   * @param {Boolean} val if the server commands have to be counted or not
   * @returns {Boolean} the new value of the attribute, or null if the value provided is invalid
   */
  set srv(val) {
    if(this._srv === undefined){
      this._srv = true;
    } 
    if(typeof val !== 'boolean') {
      return null;
    }
    this._srv = val;
    return val;
  }

  /**
   * allows the user to know if the limit is reached or not.
   * @returns {Number} 0 if the limit is reached, a Integer with the number of messages left otherwise
   */
  isValid() {
    if(this.per === 0) {
      // limit is reached, no message is allowed
      return 0;
    }
    const now = Date.now();
    const min = Date.now() - (1000 * this.per);
    const maxCount = this.rate;
    let count = 0
    let invIndex = -1;
    this.messages.forEach((unit, index) => {
      if(unit > min) {
        // the time this message is recorded is inside the delay window, it is to be considered
        count++;
      } else {
        // Out of time window, setting the index to this one to be sure to remove unused/invalid times
        invIndex = index;
      }
    });

    if(invIndex != -1) {
      // Removing old indexes
      this.messages.splice(0,invIndex+1);
    }
    // Returns the number of available messages left, or FALSE if limit is reached
    return count < maxCount ? maxCount - count : 0;
  }
  /**
   * logs that a message has been sent, for future validation using isValid()
   * @returns {void} nothing
   */
  logSending() {
    this.messages.push(Date.now());
  }
  reset() {
    this.messages = [];
  }
}