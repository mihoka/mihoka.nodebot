const RateLimiter = require('./ratelimiter');
const Helper = require('../../modules/helper');

class QueueObject {
  constructor(fnc, dat) {
    this.func = fnc;
    this.data = dat;
    this.timestamp = Date.now();
  }
}

class QueueObjectMerger {
  // TODO: Make this class, called before each poll, to merge same commands but with slightly changing data inside to be in one message only
  // (thats gonna be hard, can wait fullrelease and not indev alpha)
}

class Queue {
  constructor(rateLimiter) {
    this.enabled = true;
    this.queue = [];
    this.priorityqueue = [];
    this.rateLimiter = rateLimiter || new RateLimiter(30,30,false,false,true,true,true);
  }
  set rateLimiter (obj) {
    if(this._rateLimiter === null || this._rateLimiter === undefined) {
      this._rateLimiter = new RateLimiter();
    }
    if(!Helper.getType(obj) === 'RateLimiter') {
      return null;
    }
  }
  get rateLimiter () {
    if(this._rateLimiter === null || this._rateLimiter === undefined) {
      this._rateLimiter = new RateLimiter();
    }
    return this._rateLimiter;
  }
  enqueue(funcName, args) {
    this.queue.push(new QueueObject(funcName, args));
  }
  enqueueHighPriority(funcName, args) {
    this.priorityqueue.push(new QueueObject(funcName, args));
  }
  getAvailableQuota() {
    return this.rateLimiter.isValid();
  }
  triggerSending() {
    return this.rateLimiter.logSending();
  }
  changeRates(MessagesPerDelay, Delay) {
    this.rateLimiter.per = Delay;
    this.rateLimiter.rate = MessagesPerDelay;
  }
  poll() {
    if(this.priorityqueue.length > 0) {
      return this.priorityqueue.splice(0,1);
    }

    if(this.queue.length > 0) {
      return this.queue.splice(0,1);
    }
    return null;
  }
  areJoinPartCounted() {
    return this.rateLimiter.join;
  }
  areNickPassCounted() {
    return this.rateLimiter.conn;
  }
  isPrivMsgCounted() {
    return this.rateLimiter.mess;
  }
  areActionsCounted() {
    return this.rateLimiter.act;
  }
  areServerCommandsCounted() {
    return this.rateLimiter.srv;
  }
  purge() {
    this.rateLimiter.reset();
  }
}

module.exports = Queue;