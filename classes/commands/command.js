const Helper = require('./../../modules/helper');
class Command {
  constructor(name, type, regex, runFunc) {
    this.name = name || null;
    this.type = type || Command.CMD_TYPE_COMMAND;
    this.regex = regex || null;
    this.runFunc = runFunc;
    this.isEnabled = true;
    this._subcommands = [];
    this.calledAt = 0;
    this.permissions = {
      users: [],
      groups: ['viewers+'],
    }
  };

  static get CMD_TYPE_COMMAND() {
    return 1;
  };
  static get CMD_TYPE_TRIGGER() {
    return 2;
  };

  get name() {
    return this._name;
  };
  get type() {
    switch (this._type) {
      case Command.CMD_TYPE_COMMAND:
        return 'command';
      case Command.CMD_TYPE_TRIGGER:
        return 'trigger';
      default:
        return null;
    }
  };
  get regex() {
    return this._regex;
  };
  get runFunc() {
    return this._runFunc;
  };
  get isDelayed() {
    return this._delayed;
  };
  get delay() {
    return this._delay;
  };
  get isEnabled() {
    return this._enabled;
  };
  set name(value) {
    if (this._name === undefined) {
      this._name = null;
    }
    if (!value) {
      return false;
    }

    if (value === this._name) {
      return true;
    }

    if (typeof value !== 'string') {
      return false;
    }

    this._name = value;
  };
  set type(value) {
    if (!value) {
      return false;
    }

    if (value === this._type) {
      return true;
    }

    if (isNaN(value) && typeof value !== 'string') {
      return false;
    }

    if (!isNaN(value))  {
      switch (value) {
        case Command.CMD_TYPE_COMMAND:
        case Command.CMD_TYPE_TRIGGER:
          this._type = value;
          return true;
        default:
          this._type = Command.CMD_TYPE_COMMAND;
          return false;
      }
    }

    switch (value) {
      case 'command':
        this._type = Command.CMD_TYPE_COMMAND;
        return true;
      case 'trigger':
        this._type = Command.CMD_TYPE_TRIGGER;
        return true;
      default:
        this._type = Command.CMD_TYPE_COMMAND;
        return false;
    }
  };
  set regex(value) {
    if (this._regex === undefined) {
      this._regex = /$/i;
    }
    if (!value) {
      return false;
    }

    if (value === this._regex)  {
      return true;
    }

    if (value instanceof RegExp) {
      //TODO: verify tags, depending on command type
      this._regex = value;
    }
    return false;
  };
  set runFunc(value) {
    if (typeof value !== 'function' && this._runFunc === undefined) {
      this._runFunc = (context, user, message) => { /* command is not created yet */ };
    }
    if (!value)  { return false; }
    if (value === this._runFunc) { return true; }

    if (typeof value === 'function') {
      //TODO: verify that no illegal call to channel functions are made with .toString() ?
      //TODO: Also, add a 'require' guard
      this._runFunc = value;
      return true;
    }
    return false;
  };
  set isDelayed(value) {
    if (typeof value === 'boolean') {
      this._delayed = value;
    }
    if (this._delayed === undefined) {
      this._delayed = true;
    }
  };
  set delay(value) {
    if (this._delay === undefined) {
      this._delay = 1500;
    }

    if (!value) {
      return;
    }

    if (typeof value !== 'number' || value < 0) {
      return;
    }

    this._delay = value;
  };
  set isEnabled(value) {
    if (typeof value === 'boolean') {
      this._enabled = value;
    }
    if (this._enabled === undefined) {
      this._enabled = true;
    }
  };
  /**
   * Checks whether an object is equal to the current one
   * @param {Object} object the object to compare with
   * @returns {Boolean} true if the object equals the current one
   */
  equals(object) {
    if (!object) {
      return false;
    }

    if (object === this) {
      return true;
    }

    if (!(object instanceof Command)) {
      return false;
    }

    if (object.name === this.name) {
      return true;
    }

    return false;
  };
  /**
   * Checks wether the given string fits the command regex or not
   * @param {Object} userContext the user context, including his name and groups
   * @param {String} message the message to test into the regex
   * @returns {Boolean} true if it matches, false otherwise
   */
  isMatching(message) {
    // Verifying permissions
    /* TODO: implement
    if(this.permissions.users.indexOf(userContext.name) === -1 && this.permissions.groups.indexOf(userContext.group) === -1) {
      // User requesting the command is not in the permission list, nor in the allowed group list
      return false;
    }*/

    if (typeof message !== 'string' || message.length === 0) {
      return false;
    }
    if (!this.isEnabled) {
      return false;
    }
    return this._regex.test(message);
  };
  /**
   * Runs a command function, handling what user requested.
   * @param {ServiceModule} context the context upon where the command was called
   * @param {User} user the user object of who typed the command
   * @param {String} message the message to parse and handle
   * @returns {any} the command return value, if any.
   */
  run(context, user, message) {
    let found = null;
    if (this._subcommands.length > 0) {
      found = this._subcommands.find(sc => {
        if (sc.isMatching(message)) {
          found = sc;
          return true;
        }
      });
    }

    if (!found) {
      // There is no sub-command fitting
      if (this.isEnabled) {
        // Command is enabled, continue
        if ((this.isDelayed && this.calledAt + this.delay <= Date.now()) || !this.isDelayed) {
          // Command is delayed and valid time set, or not delayed
          this.calledAt = Date.now();
          // Setting the context with a "this"
          context.this = this;
          context.command = this;
          console.log('running function from ', this);
          return this._runFunc(context, user, message);
        }
        // Reaching here, the command is not valid enough to be called.
      }
      return false;
    } else {
      // There is a subcommand fitting
      console.log('running subcommand from ', this);
      return found.run(context, user, message);
    }
  };
  /**
   * Checks whether there is a subcommand fitting the requisites of the string message
   * useful to have multiple delays for subcommands instead of one cooldown per command
   * @param {String} commandString the message on which the command is based from
   * @returns {Boolean} true if command is found, false otherwise
   */
  isSubcommand(command) {
    if (!command) {
      return false;
    }
    if (typeof command !== 'string' && !Helper.getType(command) === 'Command') {
      return false;
    }

    let found = false;
    if (this._subcommands.length > 0) {
      this._subcommands.some(sc => {
        if (typeof command === 'string' && sc.name === command) {
          found = true;
          return true;
        }
        if (Helper.getType(command) === 'Command' && sc.equals(command)) {
          found = true;
          return true;
        }
      });
    }
    return found;
  };
  addSubCommand(command) {
    if (!(Helper.getType(command) === 'Command')) {
      return false;
    }
    if (this.isSubcommand(command))  {
      return false;
    }
    this._subcommands.push(command);
    return true;
  };
  removeSubCommand(cmd) {
    console.log('-------------');
    console.log(Helper.getType(cmd), '===', 'Command', '?')
    if (!(Helper.getType(cmd) === 'Command')) {
      console.log('---- exited to false because it is not a command', cmd);
      return false;
    }

    if (this._subcommands.length === 0) {
      return false;
    }
    let foundId = -1;
    this._subcommands.some((item, index) => {
      if (cmd.equals(item)) {
        foundId = index;
        return true;
      }
    });
    if (foundId !== -1) {
      this._subcommands.splice(foundId, 1);
      return true;
    }
    return false;
  };
  setNoDelay() {
    this.delay = 0;
    this.delayed = false;
  };
};

module.exports = Command;