const Helper = require('./../../modules/helper');

class Giveaway {
  

  constructor(channel) {
    /**
     * @var Giveaway via a specific keyword
     */
    const GA_TYPE_KEYWORD = 1;
    /**
     * @var Giveaway via any word said in chat
     */
    const GA_TYPE_RANDOM  = 2;
    /**
     * @var Everyone is enrolled, directly rolled, nothing needing user action
     */
    const GA_TYPE_INSTANT = 3;
    /**
     * @var Everyone can join the giveaway
     */
    const GA_ACCESS_VIEWERS = 10;
    /**
     * @var Giveaway will count anybody, even those who never speak. Can't use TYPE_KEYWORD or TYPE_RANDOM if this option is set
     */
    const GA_ACCESS_EVERYONE = 20;
    /**
     * @var Only subs can join the giveway. If used with TYPE_VIEWERS, subs get affected by a increased luck chance factor defined with setSubscriberFactor
     */
    const GA_ACCESS_SUBS = 30;
    /**
     * @var Giveaway will count only people who spoke in chat recently
     */
    const GA_ACCESS_CHATTERS = 40;
    /**
     * @var Giveaway will only count VIPs
     */
    const GA_ACCESS_VIP = 50;

    /** 
     * @var the channel with who the giveaway is linked 
     */
    this._channel = channel;
    /**
     * @var the date of the giveaway initialization
     */
    this._startDate = null;
    /**
     * @var the date of when the giveaway went fully closed
     */
    this._endDate = null;
    /**
     * @var The array of users who won the Giveaway
     */
    this._winnersPool = [];
    /**
     * @var The array of users who participated to the giveway
     */
    this._membersPool = [];
    /**
     * @var the type of giveaway. Must be a giveaway const starting with GA_TYPE_.
     */
    this._type = 0;
    /**
     * @var the access type of the giveaway. Must start with GA_ACCESS_.
     */
    this._access = 0;
    /** 
     * @var the multiplication factor of chances to win as a Subscriber
     */
    this._subscriberFactor  = 1.5;
    this._vipFactor         = 1.2;
    this._modFactor         = 2.0;
    /**
     * @var the optionnal description of the giveway (prize to win for instance)
     */
    this._description = null;
    /**
     * @var the unique ID of the giveaway, (obtained once saved in database)?/(generated on creation)?
     */
    this.id = Helper.generateGUID();
  };
  start(type, access) {
    this._startDate = Date.now();
    this.type = type;
    this.access = access;
  };
  draw() {
    if(this._endDate != null ) {
      this.end();
    }

    const drawPool = [];
    this._membersPool.forEach(member => {
      // check luck factor
      // add x times to array, 0.1 being one entry
      // no factor means 10 registeries in the drawPool
    });
    return null;
  }
  end() {
    this._endDate = Date.now();
  };
  register(user, options) {
    const userObject = {
      user: user,
      date: helper.getTime(),
      options: options,
    }
    if( this.isRegisterable(userObject) && this._endDate == null ) {
      this.membersPool.push(userObject);
      return true;
    }
    return false;
  };
  isRegistered(user) {
    const membersFound = this.membersPool.filter((item) => {
      return item.user.name == user.name;
    });
    return membersFound.length > 0;
  };
  isRegisterable(user) {
    // Can't register if already registered
    if(this.isRegistered(user)) { return false; }
    // User is a moderator of channel, he is able to register
    if(user.tags.isMod) { return true; }
    // User is a subscriber, he is able to register if its not only for VIPs
    switch(this.access) {
      case Giveaway.GA_ACCESS_VIP:
        return this._channel.isVip(user);
        break;
      case Giveaway.GA_ACCESS_SUBS:
        return user.tags.subscriber;
        break;
      case Giveaway.GA_ACCESS_CHATTERS: 
        // TODO: fix this one, check if he talked recently
        break;
      case Giveaway.GA_ACCESS_VIEWERS:
      case Giveaway.GA_ACCESS_EVERYONE:
        return true;
        break;
    }
    return false;
  }
  set type(value) {
    switch(value) {
      case Giveaway.GA_TYPE_INSTANT:
      case Giveaway.GA_TYPE_KEYWORD:
      case Giveaway.GA_TYPE_RANDOM:
        this._type = value;
        break;
    }
    return false;
  }
  set access(value) {
    switch(value) {
      case Giveaway.GA_ACCESS_CHATTERS:
      case Giveaway.GA_ACCESS_EVERYONE:
      case Giveaway.GA_ACCESS_VIEWERS:
      case Giveaway.GA_ACCESS_SUBS:
      case Giveaway.GA_ACCESS_VIP:
        this._access = value;
        break;
    }
    return false;
  }
  get type() {
    return this._type;
  }
  get access() {
    return this._access;
  }
}