const global_config = require('./../../config');
const Helper = require('./../../modules/helper');
const __nativeCommands = [
    {
        name: '!commands',
        //TODO: Add aliases for command
        type: 'command', // 'command', 'trigger'
        run: (channel, user, message) => {
            channel.irc.sendMessage("Vous pouvez retrouver les commandes de la chaine sur https://mihoka.tv/u/" + (channel.name.toString().substring(1)) + "/commands");
        },
        call: 0,
        delayed: true,
        delay: 10000,
        regex: /^!(commande?s(\s|$)|cmdlist$|liste?$)/i,
        enabled: true,
    },
    {
        name: '!fc',
        type: 'command',
        run: (channel, user, message) => {
            //TODO: set the global API var
            // Call API here for kraken.twitch.tv/api/user/{user.nickname}/following/{channel.toString().substring(1)}
            channel.irc.sendMessage("Impossible de contacter le service d'API. Veuillez réessayer plus tard.");
        },
        call: 0,
        delayed: true,
        delay: 5000,
        regex: /^!fc(\s(.+))?/i,
        enabled: true,
    },
    {
        name:'!quote',
        type:'command',
        run: (channel, user, message) => {
            let params = Helper.parseCommandParameters(message);
            // Maybe refactor commands structure to have "subcommands" (add/remove/...) with their own delay?
            if(params.length > 1) {
                switch(params[1]) {
                    case 'add':
                        // adding a new quote
                        if(params.length < 3) { return false; }
                        let quote = {
                            by: user,
                            date: Date.now(),
                            popularity: 0,
                            message: params.slice(2, params.length).join(' '),
                        }
                        const indexFound = Helper.arrayObjectIndex(quote,channel.quotes,'message');
                        if(indexFound === -1) {
                            // Add as it is not existing already
                            channel.quotes.push(quote);
                            //TODO: Confirm by message it has been added
                        } else {
                            //TODO: "It already exists"
                        }
                        break;
                    case 'remove':
                        // removing a quote by Id or date
                        if(params.length == 3) {
                            try {
                                const idRecord = Number.parseInt(params[2]);
                                if(idRecord >= channel.quotes.length) {
                                    // invalid ID
                                    throw new Error('out of bound index');
                                }
                                const removedValue = channel.quotes.splice(idRecord, 1);
                                //TODO: confirm suppression of removedValue.message
                            } catch(e){
                                //TODO: invalid ID format
                            } 
                        }
                        break;
                    case 'cancel':
                        // removes last quote of array
                        channel.quotes.pop();
                        break;
                    case 'clear':
                        // clean whole array
                        channel.quotes = [];
                        break;
                }
            } else {
                // Poll a unpopular quote to display it
                channel.sendMessage('#9193 - '+ channel.quotes[0].date +' by ' + channel.quotes[0].by + ' - ' + channel.quotes[0].message);
            }
        },
        call: 0,
        delayed:false,
        delay: 10000,
        regex: /^!quote(\sadd\s(.*)+|\sremove\s[0-9]+|\scancel)?/i,
        enabled: true,
    },
    {
        name: '!welcome',
        type: 'command',
        run: (channel, user, message) => {
            let _params = Helper.parseCommandParameters(message);
            switch (_params[1]) {
                case 'mode':
                    const welcomeTypes = {
                        '0': 'no longer welcome anybody when they arrive',
                        '1': 'welcome anyone that joins the chat',
                        '2': 'welcome anyone that speaks for the first time (since specified delay)',
                        '3': 'welcome anyone that speaks for the first time (ever)',
                        '4': 'welcome only those who are on the VIP list',
                        '5': 'welcome only those who have a custom welcome message set',
                        '6': 'welcome only moderators',
                    };
                    const arrayKeys = Array.keys(welcomeTypes);
                    if(_params[2] && arrayKeys[_params[2]]) {
                        // The parameter exists in the available welcome modes
                        const validated = channel.addSetting('MHK_WKM_MODE', _params[2]);
                        if(validated) {
                            // Setting has been added
                            channel.sendMessage(`I will ${welcomeTypes[_params[2]]}`);
                        } else {
                            // Failed to add the setting
                            channel.sendMessage(`Unable to update my settings concerning the welcome message. Please try again later!`);
                        }
                    }
                    break;
                case 'message':
                    if(_params[2]) {
                        const welcomeMessage = _params.slice(2).join(' ');
                        if(welcomeMessage.length > 0) {
                            const validated = channel.addSetting('MHK_WKM_MESSAGE', welcomeMessage);
                            if(validated) {
                                channel.sendMessage('The welcome message is now updated.');
                            } else {
                                channel.sendMessage('It seems like I failed to save this welcome message... Sorry FailFish');
                            }
                        } 
                    }
                    break;
                case 'delay':
                    if (!isNaN(_params[2])) {
                        const delayProvided = Math.round(_params[2]) * 1000;
                        const validated = channel.addSetting('MHK_WKM_DELAY', delayProvided);
                        if(validated) {
                            channel.sendMessage('The delay has been updated ! VoHiYo');
                        } else {
                            channel.sendMessage('I was unable to save the delay into my parameters. Sorry !');
                        }
                    }
                    break;
            }
        },
        call: 0,
        delayed: true,
        delay: 1500,
        regex: /^!welcome\s(mode|message|delay)\s(.+)/i,
        enabled: true,
    },
    {
        name: '!command',
        type: 'command',
        run: (channel, user, message) => {
            let _params = Helper.parseCommandParameters(message);
            let commandKey = _params.slice(2).join(' ');
            switch (_params[1]) {
                case 'add':
                    // Adding a command
                    if (_params[2] && _params[3]) {
                        //TODO: change with a Command class object

                        // Logic: If the command name is not starting with ! it's a trigger, who can be multi-word, 
                        // so if it contains " we need to get only what is in between as a name for the regex
                        let commandToAdd = {};
                        commandToAdd.type = _params[2].substring(0, 1) === '!' ? 'command' : 'trigger';
                        commandToAdd.name = _params[2];

                        const multiwordRegex = /^!command add \"([^\"]+)\"\s/i;
                        const returnedResult = multiwordRegex.exec(message);
                        let isMultiWord    = false;
                        // Checking if the command is multi word trigger or not
                        if(returnedResult !== null) {
                            // Contains the group of the regex
                            commandToAdd.name = returnedResult[1];
                            commandToAdd.regex = new RegExp(returnedResult[1],'i');
                            isMultiWord = true;
                        } else {
                            // Does not fit the multiline-trigger style
                            commandToAdd.regex = new RegExp(commandToAdd.name,i);
                        }
                        commandToAdd.enabled = true;
                        commandToAdd.run = (channel, user, message) => {
                            // Prints the message set by the command, we remove the command name length (+2 for triggers with ")
                            channel.irc.sendMessage(
                                commandKey.substring( commandToAdd.name.length 
                            +   (isMultiWord ? 2 : 0)
                            ));
                        };
                        
                        if(channel.addCommand(commandToAdd)) {
                            channel.sendMessage(`La commande ${_params[2]} a été créée avec succès !`)
                        } else {
                            channel.sendMessage(`J'ai été dans l'incapacité de créer la commande, navrée!`)
                        }
                    }
                    else {
                        // We need at least 4 space-separated words, invalid format.
                        return false;
                    }
                    break;
                case 'remove':
                    // Deleting a command from array
                    if(channel.removeCommand(commandKey)) {
                        // Success of deletion
                        channel.sendMessage(`La commande '${commandKey}' a été supprimée.`)
                    }
                    else {
                        // failed
                        channel.sendMessage('La suppression de la commande demandée a échouée. Est-ce un bon nom de commande?');
                    }
                    break;
                case 'enable':
                    // Enables a command previously disabled. No effect if command is already enabled.
                    if(commandKey.substring(0,1) === '"') {
                        commandKey = commandKey.substring(1,commandKey.length -1);
                    }
                    let _command = channel.findCommandByName(commandKey);
                    if(_command !== null) {
                        _command.enabled = true;
                        if(channel.updateCommand(_command)) {
                            channel.sendMessage('The command has been enabled');
                        }
                    } else {
                        channel.sendMessage(`No command with that name found: ${commandKey}`)
                    }
                    break;
                case 'disable':
                    // Disables an enabled command. She won't be callable anymore until enabled again.
                    if(commandKey.substring(0,1) === '"') {
                        commandKey = commandKey.substring(1,commandKey.length -1);
                    }
                    let commandFound = channel.findCommandByName(commandKey);
                    if(commandFound !== null) {
                        commandFound.enabled = false;
                        if(channel.updateCommand(commandFound)) {
                            channel.sendMessage('The command has been disabled');
                        }
                    } else {
                        channel.sendMessage(`No command with that name found: ${commandKey}`)
                    }
                    break;
                case 'clear':
                    // Removes ALL user-made commands.
                    channel.clearCommands();
                    channel.sendMessage('Toutes les commandes ont été effacées.')
                    break;
                default:
                    // No case to handle the parameter.
                    break;
            }
        },
        call: 0,
        delayed: true,
        delay: 1000,
        regex: /^!commande?\s(add|remove|enable|disable|clear)\s(\S+)/i,
        enabled: true,
    },
    {
        name: '!mihoka',
        type: 'command',
        run: (channel, user, message) => {
            let _params = Helper.parseCommandParameters(message);
            if (_params.length > 1 && _params[1] !== '') {
                // This is a typed command, it needs to handle the type
                switch (_params[1]) {
                    case 'leave':
                        // Leave the channel
                        channel.sendMessage(`Très bien. Je quitte le chat. Vous pouvez me réactiver en vous rendant sur https://mihoka.tv/. Merci d'avoir utilisé mes services!`);
                        channel.disconnectIrc();
                        channel.addSetting('MHK_IRC_AUTOCONNECT', false);
                        break;
                    case 'silent':
                        // Still do timeouts, but don't answer commands
                        channel.sendMessage(`Mode silencieux activé. Tapez '${name} enable' pour réactiver les messages de Mihoka.`);
                        channel.addSetting('MHK_MODE_SILENT', true);
                        break;
                    case 'enable':
                        // Enables Mihoka functionnalities
                        this.channel.addSetting('MHK_MODE_SILENT', false);
                        this.channel.addSetting('MHK_MODE_ACTIVE', true);
                        this.channel.sendMessage(`Mihoka activée ! ... Initialisation terminée! Prête à vous assister! VoHiYo`);
                        break;
                    case 'disable':
                        // Disable any chat interaction, even timeouts
                        this.channel.sendMessage(`Désactivation des composants et du système... Tapez '${this.name} enable' pour me réactiver.`);
                        this.channel.addSetting('MHK_MODE_ACTIVE', false);
                        break;
                    case 'delay':
                        // Define the delay between two commands to be executed
                        try {
                            let delay = Number.parseInt(_params[2]);
                            if (delay <= 0) { throw 'Only positive numbers allowed.'; }
                            delay *= 1e3;
                            this.channel.addSetting('MHK_COMMANDS_DELAY', delay);
                            this.channel.sendMessage(`Le délai entre deux commandes est désormais de ${delay * 1e-3}`)
                        } catch (e) {
                            console.error('MHK-BOT:/classes/commands/nativeCommands.js: Error while parsing the integer value.', e);
                            this.channel.sendMessage(`Invalid value for the delay, please provide a positive number of seconds.`);
                        }
                        break;
                    case 'links':
                        // Enable or disable links to be allowed in chat
                        let _params = Helper.parseCommandParameters(message);
                        if (_params[2]) {
                            if (_params[2] == 'true' || _params[2] == '1' || _params[2] == 'on') {
                                // Enable links
                                channel.addSetting('MHK_BOT_LINKS', true);
                                channel.sendMessage('Les liens sont désormais désactivés pour la chaine. toute personne non autorisée se vera être timeout.');
                            }
                            else if (_params[2] == 'false' || _params[2] == '0' || _params[2] == 'off') {
                                // Disable links
                                channel.addSetting('MHK_BOT_LINKS', false);
                                channel.sendMessage('Les liens sont désormais activés sur la chaine, aucune sanction ne sera prise si un lien est posté.');
                            }
                            else {
                                // Invalid input
                            }
                        }
                        break;
                    case 'lang':
                        if(_params[2]) {
                            if(channel.addSetting('MHK_BOT_LANGUAGE', _params[2])) {
                                channel.sendMessage(`I will now answer to you in ${_params[2]} !`);
                            } else {
                                channel.sendMessage('Sorry but I failed to change your language. Maybe try with another value?');
                            }
                        }
                        break;
                    default:
                        // This case wasn't caught, it's probably an invalid parameter entered
                        console.error(`Uncaught command requested in NativeCommands:`, {user,channel,message })
                        break;
                }
            }
            else {
                // This is a basic command, it has to display the Mihoka information message
                channel.sendMessage(`Je suis Mihoka, une entité créée pour faciliter la vie des streamers. Ma version actuelle est ${global_config.app.version}. Vous pouvez en savoir plus sur moi en vous rendant sur https://mihoka.tv/.`);
            }
        },
        call: 0,
        delayed: true,
        delay: 1000,
        regex: /^!mihoka(\sleave|\ssilent|\senable|\sdisable|\sdelay\s\d|\slinks\s(on|off|1|0|true|false)|lang\s[a-zA-Z]{0,3}|$)/i,
        enabled: true,
    },
    {
        name: '!permit',
        type: 'command',
        run: (channel, user, message) => {
            if(channel.addPermit(user)) {
                channel.sendMessage(`L'utilisateur ${user} peut désormais poster un lien dans les 3 prochaines minutes sans être timeout.`);
            } else {
                channel.sendMessage(`Erreur lors de l'ajout de ${user} à la liste des utilisateurs autorisés.`);
            }
        },
        call: 0,
        delay: 1000,
        delayed: true,
        regex: /^!permit\s(\S+)/i,
        enabled: true,
    },
    {
        name: 'linkPosted',
        type: 'trigger',
        run: (channel, user, message) => {
            if(!channel.isPermitted(user)) {
                // Timeout the user
                channel.sendMessage(`.timeout ${user} 1 the user is not permitted to post links`);
            }
        },
        call: 0,
        delayed: true,
        delay: 0,
        regex: /(?:https?:\/\/)?(?:[-a-zA-Z0-9@:%+~#=]+\.)+[a-z]{2,6}\b(?:[-a-zA-Z0-9@:%+.~#?&\/=]*)/gmi,
        enabled: true,
    },
    {
        name: '!vip',
        type: 'command',
        run: (channel, user, message) => {
            vipName = channel.getSetting('MHK_VIP_NAME') || 'VIP';
            let _params = Helper.parseCommandParameters(message);
            // TODO: Make sure user don't enter .me or /me in the output text or we are SCREWED. Same goes for /Color (if using MihokaTV bot and not a custom one), /mod, etc.
            switch (_params[1]) {
                case 'add':
                    // Adding a vip
                    if (_params.length < 3) { return; }
                    channel.addVip(_params[2]);
                    channel.sendMessage(`L'utilisateur ${_params[2]} a été ajouté de la liste des ${vipName}.`);
                    break;
                case 'remove':
                    // Removing a vip
                    if (_params.length < 3) { return; }
                    channel.removeVip(_params[2]);
                    channel.sendMessage(`L'utilisateur ${_params[2]} a été retiré de la liste des ${vipName}.`);
                    break;
                case 'message':
                    // Setting welcome message for VIP
                    if (_params.length < 3) { return; }
                    const vipMessage = Helper.sanitizeText(_params.slice(2).join(' '));
                    if (vipMessage.length > 0) {
                        channel.addSetting('MHK_VIP_MESSAGE', vipMessage);
                        channel.sendMessage(`Le message pour les ${vipName} a été mis à jour !`);
                    }
                    break;
                case 'enable':
                    if (_params.length < 3) { return; }
                    // Enables VIP messages 
                    channel.addSetting('MHK_VIP_ENABLED', true);
                    channel.sendMessage('Mihoka activée! Je suis prête à répondre à vos commandes et sanctionner les troubles-fête! VoHiYo');
                    break;
                case 'disable':
                    // Disables VIP messages
                    if (_params.length < 3) { return; }
                    channel.addSetting('MHK_VIP_ENABLED', false);
                    break;
                default:
                    // Not an handled case
                    break;
            }
        },
        call: 0,
        delay: 1500,
        delayed: false,
        regex: /^!vip\s(add|remove|enable|disable|message)\s(.+)/i,
        enabled: true,
    },
    {
        name: '!roll',
        type: 'command',
        run: (channel, user, message) => {
            let _params = Helper.parseCommandParameters(message);
            let minRollValue = 1;
            let maxRollValue = 100;
            if (_params[1]) {
                // Parameter has been added
                if (/[0-9]+\-[0-9]+/i.exec(_params[1]) !== null) {
                    // This is a range-roll
                    _pSplit = _params[1].split('-');
                    if (_pSplit.length > 1 && _pSplit[1] != '') {
                        try {
                            minRollValue = Number.parseInt(_pSplit[0])
                            // First roll number is ok
                            try {
                                maxRollValue = Number.parseInt(_pSplit[1]);
                                // Second roll number is ok
                            } catch (e) {
                                // Invalid format for second number
                                // Reverting to default
                                minRollValue = 1;
                                maxRollValue = 100;
                            }
                        } catch (e) {
                            // Invalid format for first number
                            // Reverting to default
                            minRollValue = 1;
                            maxRollValue = 100;
                        }
                    }
                }
                else {
                    // this is a 1 to {param} roll
                    maxRollValue = Number.parseInt(_params[1]);
                }
                return Math.floor(minRollValue + (Math.random() * (maxRollValue + 1) - minRollValue));
            }
        },
        call: 0,
        delayed: false,
        delay: 20000,
        regex: /^!roll(\s\S+)?/i,
        enabled: true,
    },
    {
        //TODO: Add aliases for each commands so users can do their own calls like !rip for !count
        name: '!count',
        type: 'command',
        run: (channel, user, message) => {
            const params = Helper.parseCommandParameters(message);
            switch(params.length){
                case 1:
                // Adding to count a death, short call
                break;
                case 2:
                // either 'reset' or a INT, with death additions value
                break;
            } 
        },
        call: 0,
        delay: 5000,
        delayed: false,
        regex: /^!count(\s(reset|-?[0-9]+))/i,
        enabled: true,
    },
    {
        name: '!advert',
        type: 'command',
        run: (channel, user, message) => {
            // Advert management
            const params = Helper.parseCommandParameters(message);
            switch(params[1]){
                case 'add':
                // Adding a new messages
                if(params.length > 3) {
                    const advert = {
                        key: params[2],
                        ad: Helper.sanitizeText(params.slice(3, params.length).join(' ')),
                    }
                    channel.addAdvert(advert);
                } else {
                    //TODO: wrong format, !advert add key message
                }
                break;
                case 'remove':
                // Removing an advert by unique-id
                break;
                case 'count':
                // sends the count of adverts existing 
                const count = channel.adverts.length;
                break;
                case 'clear':
                // empty the array
                channel.adverts.clear();
                break;
            }
        },
        call: 0,
        delayed: true,
        delay: 1000,
        regex: /^!advert (add\s(.*)+|remove\s[0-9]+|count|clear|id\s(on|off))/i,
        enabled: true,
    }
];
module.exports = __nativeCommands
