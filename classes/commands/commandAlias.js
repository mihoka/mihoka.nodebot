const Command = require('./command');
const Helper = require('./../../modules/helper');
/**
 * Creates a command alias. Allows users to type specific words as commands or tirggers, and to call another command instead.
 * Warning, the command must fit exactly exactly the same parameters if appliedChanges is not defined. If it is defined,
 * appliedChanges will be called to alter the message of the user and then sends the alterated value to the real command.
 */
class CommandAlias extends Command {
  constructor(regex, aliasOf, appliedChanges) {
    if(!(aliasOf instanceof Command)) {
      aliasOf = new Command();
    }
    super(null, 'command', regex, null, true, 1500, true)
    this._alias = aliasOf;
    this._changes = appliedChanges;
  }
  run(context,user,message) {
    const postchanges = this._changes(message);
    super.run(context, user, postchanges);
  }
  getSource() {
    return this._alias;
  }
  getChanges() {
    return this._changes;
  }
}