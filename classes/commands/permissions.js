module.exports = class Permissions {
  constructor(){}
  static get VIEWERS_AND_HIGHER () {
    return 'viewers+';
  }
  static get REGULARS_AND_HIGHER () {
    return 'regulars+';
  }
  static get SUBS_AND_HIGHER () {
    return 'subscribers+';
  }
  static get MODERATORS_AND_HIGHER () {
    return 'moderators+';
  }
  static get EDITORS_AND_HIGHER () {
    return 'editors+';
  }

  static get VIEWERS () {
    return 'viewers';
  }
  static get REGULARS () {
    return 'regulars';
  }
  static get SUBSCRIBERS () {
    return 'subscribers';
  }
  static get MODERATORS () {
    return 'moderators';
  }
  static get EDITORS () {
    return 'editors';
  }
  static get BROADCASTER () {
    return 'broadcaster';
  }
  static get BOTS () {
    return 'bots';
  }
}