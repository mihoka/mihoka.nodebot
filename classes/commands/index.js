const Command       = require('./command');
const CommandAlias  = require('./commandAlias');
const Advert        = require('./advert');
const Giveaway      = require('./giveaway');
const Vip           = require('./vip');

module.exports = {
  Vip, Giveaway, Advert, Command, CommandAlias
}