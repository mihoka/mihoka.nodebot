class VIP {
  constructor(id) {
    this.userId = isNaN(id) ? -1 : id;
    this.createdAt = Date.now();
  }
}