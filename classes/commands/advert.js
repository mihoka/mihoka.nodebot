const Helper = require('../../modules/helper');

class Advert {
  constructor(key, message) {
    this.createdAt = Date.now();
    this.uniqueKey = key;
    this.message   = Helper.sanitizeText(message);
  }
  equals(obj) {
    if(this === obj) {
      return true;
    }

    if(!obj) {
      return false;
    }

    if(typeof obj != 'function') {
      return false;
    }

    if(!(obj instanceof Advert)) {
      return false;
    }

    if(typeof obj.equals === 'function' && obj.uniqueKey == this.uniqueKey) {
      return true;
    }

    return false;
  }
  /**
   * Fetches the advert message
   * @returns {String} the advert message
   */
  get() {
    return this.message;
  }
}