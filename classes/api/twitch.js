const Api = require('./api');
const ApiRequest = require('./request');

class API_Twitch {
  static create(authorization) {
    const request = new Api(__GLOBAL__.api.twitch.uri, __GLOBAL__.api.twitch.port);
    request.setSecured(true);
    request.setBase(__GLOBAL__.api.twitch.base);
    request.headers['Authorization'] = authorization || __GLOBAL__.api.twitch.Authorization;
    request.headers['Accept'] = 'application/vnd.twitchtv.v5+json';
    request.headers['Client-ID'] = __GLOBAL__.api.twitch.client_id;
    return request;
  };
  static isFollowing(user, channel) {
    const obj = create(channel.authorization);
    const req = obj.prepare('/users/' + user.id + '/follows/channels/' + channel.id, 'GET');
    req.exec().then((rsp) => {
      // Success
      if (rsp.error) {
        return false;
      }
      return true;
    }).catch((err) => {
      console.error('MHK-API/Twitch'.red, 'checkUserFollowsByChannel()'.cyan, err.message);
    });
  };
}