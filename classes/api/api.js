const ApiRequest = require('./request');

class Api {
  constructor(domain, port) {
    this.host = domain;
    this.port = port;
    this.base = '/';
    this.secured = true;
    this.headers = {};
    this.request = null;
  }
  setHostname(uri) {
    this.host = uri;
    // TODO: add Regex parsing the url if the user type http(s)://
  }
  setPort(port) {
    this.port = port;
    //TODO: check port is numeric integer
  }
  setBase(base) {
    this.base = base;
  }
  setSecured(bool) {
    if (typeof bool !== 'boolean') {
      return false;
    }
    this.secured = bool;
  }
  prepare(url, method, options) {
    this.request = new ApiRequest(this.host, this.port, this.secured);
    this.request.setURI(this.base + url);
    this.request.setMethod(method);
    this.request.addHeaders(this.headers);
    return this.request;
  }
  isValid() {
    return this.request !== null;
  }
  getRequest() {
    return this.request || this.prepare();
  }
}

module.exports = Api;