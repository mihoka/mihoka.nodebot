const http = require('http');
const https = require('https');
class ApiRequest {
  constructor(hostname, port, ssl) {
    this.hostname = hostname;
    this.port = port;
    this.secured = typeof ssl === 'boolean' ? ssl : true;
    this.requestPath = '/';
    this.agent = null;
    this.headers = {};
    this.result = {};
    this.method = 'GET';
    this.body = '';
  }
  addHeader(key, value) {
    this.headers[key] = value;
  }
  addHeaders(object) {
    this.headers = Object.assign({}, this.headers, object);
  }
  setAgent(object) {
    this.agent = object;
  }
  setURI(uri) {
    this.requestPath = uri;
  }
  setMethod(method) {
    // TODO check validity
    this.method = method;
  }
  setBody(content) {
    this.body = content;
  }
  exec() {
    const options = {
      port: this.port,
      host: this.hostname,
      path: this.requestPath,
      method: this.method,
      headers: this.headers,
    };
    return new Promise((resolve, reject) => {

      const req = (this.secured ? https : http).request(options, (res) => {
        this.result.body = '';
        this.result.status = res.statusCode;
        this.result.headers = res.headers;
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
          this.result.body += chunk;
        });
        res.on('end', () => {
          let rsp;
          try {
            rsp = JSON.parse(this.result);
            resolve(rsp);
          } catch (e) {
            rsp = { error: 'PARSE_FAILED', message: e };
            reject(rsp);
          }
        });
      });
      req.on('error', (err) => {
        return reject({ error: 'ERROR', message: err });
      });
      req.on('timeout', () => {
        return reject({ error: 'TIMEOUT', message: 'Request timed out.' });
      });
      req.setTimeout(2000);
      req.write(this.body);
      req.end();
    });
  }
}

module.exports = ApiRequest;