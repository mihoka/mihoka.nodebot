const Channel = require('./../../modules/channel');

class CommandTools {
  static executeTest(module, moduleData, outputArray) {
    // to be defined, we should create channel, user, and do all the work for the module to be testable, and return everything
    const that_channel = new Channel('testChannel');
    that_channel.addModule(module);
    that_channel.setModuleData(module.name, moduleData);
    return that_channel;
  };
}

module.exports = CommandTools;