module.exports = {
    dev: {
        chat: true,
        debug: true,
    },
    app: {
        name: 'Mihoka',
        version: '0.1.2 (indev)',
        date: '2017-04-19 11:51 GMT+0',
    },
    sql: {
        host: '10.30.30.10',
        port: 5432,
        user: 'mihoka',
        password: 'secret',
        database: 'mihoka',
    },
    irc: {
        twitch: {
            user: {
                nick: "justinfan111111",
                pass: "",
            },
            server: {
                host: 'irc.chat.twitch.tv',
                port: 6667,
            },
        },
    },
    api: {
        twitch: {
            uri: 'api.twitch.tv',
            base: '/kraken',
            port: 443,
            secured: true,
            client_id: "2hsqajnnemvex685fhyd2gh4iy4ca56",
        },
    },
};